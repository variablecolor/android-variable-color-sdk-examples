package com.variable.example;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.DashPathEffect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.variable.bluetooth.ColorInstrument;
import com.variable.bluetooth.OnButtonPressListener;
import com.variable.bluetooth.OnColorCaptureListener;
import com.variable.bluetooth.spectro.SpectralPoint;
import com.variable.color.ColorScan;
import com.variable.color.GlossMeasurement;
import com.variable.color.Illuminants;
import com.variable.color.LabColor;
import com.variable.color.Observer;
import com.variable.color.SpectralCurve;
import com.variable.error.OnErrorListener;
import com.variable.error.VariableException;
import com.variable.example.util.ActivityUtil;
import com.variable.example.util.MapToJsonConverter;
import com.variable.util.json.JSONSerializer;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class ColorScanningActivity extends AppCompatActivity implements OnColorCaptureListener, OnErrorListener, OnButtonPressListener {
    private Illuminants mSelectedIlluminant = Illuminants.D50;
    private Observer mSelectedObserver = Observer.TWO_DEGREE;
    private ColorScan scan;

    private ColorInstrument getConnectedPeripheral(){
        return ((SampleApplication) getApplication()).getVariableSDK().getConnectionManager().getConnectedPeripheral();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scanning);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        ColorInstrument instrument = getConnectedPeripheral();
        if(instrument != null && ColorInstrument.isSpectro2(instrument.getModel())){
            findViewById(R.id.scan_mode_container).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.btnScanColors).setOnClickListener(this::requestScan);

        ((androidx.appcompat.widget.SwitchCompat) findViewById(R.id.switch_observer)).setOnCheckedChangeListener(this::onObserverChange);
        ((androidx.appcompat.widget.SwitchCompat) findViewById(R.id.switch_illuminant)).setOnCheckedChangeListener(this::onIlluminantChange);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.action_share){
            if(this.scan == null) {
                return false;
            }


            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND); // Use ACTION_SEND for a single file

            String jsonData = MapToJsonConverter.mapToJsonObject(this.scan.toMap()).toString();

            try {
                File jsonFile = File.createTempFile("scan_data", ".json");
                FileOutputStream outputStream = new FileOutputStream(jsonFile);
                outputStream.write(jsonData.getBytes());
                outputStream.close();



                Uri fileToAttachUri = FileProvider.getUriForFile(
                        this,
                        "com.variable.examples.fileprovider", // Replace with your authority
                        jsonFile);

                sendIntent.putExtra(Intent.EXTRA_STREAM, fileToAttachUri);
                sendIntent.setType("application/json");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);

            } catch (IOException e) {
                // Handle the exception (e.g., show an error message)
                e.printStackTrace();
            }
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ColorInstrument instrument = getConnectedPeripheral();
        if(instrument != null){
            instrument.setButtonPressListener(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        ColorInstrument instrument = getConnectedPeripheral();
        if(instrument != null){
            instrument.setButtonPressListener(null);
        }
    }


    @Override
    public void onButtonPress(@NonNull ColorInstrument colorInstrument) {
        requestScan(findViewById(R.id.btnScanColors));
    }

    private void requestScan(@NonNull  View view) {
        // Get the colorimeter... null is no connection... possibly remote disconnection if keep alive is off.
        ColorInstrument colorInstrument = getConnectedPeripheral();
        if (colorInstrument == null) {
            Toast.makeText(this, "device not connected", Toast.LENGTH_SHORT).show();
            return;
        }

        if(ColorInstrument.isSpectro2(colorInstrument.getModel())){
            int id = ((RadioGroup) findViewById(R.id.scan_mode_container)).getCheckedRadioButtonId();
            if (id == R.id.m0) {
                colorInstrument.requestColorScan(ColorInstrument.MeasurementModes.m0, ColorScanningActivity.this, this);
            } else if (id == R.id.m1) {
                colorInstrument.requestColorScan(ColorInstrument.MeasurementModes.m1, ColorScanningActivity.this, this);
            } else if (id == R.id.m2) {
                colorInstrument.requestColorScan(ColorInstrument.MeasurementModes.m2, ColorScanningActivity.this, this);
            }
        }else {
            colorInstrument.requestColorScan(ColorScanningActivity.this, this);
        }
    }

    private void onObserverChange(CompoundButton compoundButton, boolean isTenDegree) {
        if (isTenDegree) {
            mSelectedObserver = Observer.TEN_DEGREE;
            compoundButton.setText("Observer 10°");
        } else {
            mSelectedObserver = Observer.TWO_DEGREE;
            compoundButton.setText("Observer 2°");
        }
    }

    private void onIlluminantChange(CompoundButton compoundButton, boolean isD65) {
        if (isD65) {
            mSelectedIlluminant = Illuminants.D65;
            compoundButton.setText("Illuminant D65");
        } else {
            mSelectedIlluminant = Illuminants.D50;
            compoundButton.setText("Illuminant D50");
        }
    }

    @Override
    public void onError(@NonNull VariableException ex) {
        if (ex.getCode() == VariableException.DEVICE_NOT_CONNECTED) {
            Toast.makeText(this, "Device not connected", Toast.LENGTH_SHORT).show();
            return;
        }

        Log.e(BuildConfig.TAG, "DEVICE NOT CALIBRATED");
        new AlertDialog.Builder(this)
                .setTitle("Device not calibrated")
                .setMessage("Place cap onto the device before continuing")
                .setPositiveButton("Calibrate", (dialog, which) -> {
                    startActivity(new Intent(this, CalibrationActivity.class));
                })
                .show();
    }

    private void onGlossCapture(@Nullable  GlossMeasurement measurement) {
        TextView txtGloss = findViewById(R.id.txtGloss);
        if (measurement == null) {
            txtGloss.setText("Not Applicable");
        } else {
            findViewById(R.id.txtGloss).setVisibility(View.VISIBLE);
            txtGloss.setText(String.format(Locale.getDefault(), "%.2f", measurement.getGloss() * 100));
        }
    }

    @Override
    public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
        this.scan = scan;

        LabColor color = scan.getAdjustedLabColor(mSelectedIlluminant, mSelectedObserver);

        this.onGlossCapture(scan.getGloss());

        ((TextView) findViewById(R.id.txtHex)).setText(String.format("Hex: %s", color.toHex()));
        ((TextView) findViewById(R.id.txtLab)).setText(color.toString());
        findViewById(R.id.color).setBackgroundColor(color.toColor());

        findViewById(R.id.spectrum_chart).setVisibility(View.GONE);
    }

    @Override
    public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
        this.scan = scan;

        SpectralCurve curve = Objects.requireNonNull(scan.getSpectralCurve(), "curve is missing");
        LabColor color = curve.toLab(mSelectedIlluminant, mSelectedObserver);
        int rgbColor = color.toColor();

        this.onGlossCapture(scan.getGloss());

        ((TextView) findViewById(R.id.txtHex)).setText(String.format("Hex: %s", color.toHex()));
        ((TextView) findViewById(R.id.txtLab)).setText(color.toString());

        List<SpectralPoint> intensities = Objects.requireNonNull(scan.getSpectralCurve(), "curve is missing");
        float maxValue = 0.1f;
        ArrayList<Entry> values = new ArrayList<>();
        for(SpectralPoint p : intensities){
            values.add(new Entry(p.getWavelength(), p.getIntensity()));

            maxValue = Math.max(p.getIntensity(), maxValue);
        }


        LineChart spectrumChart = findViewById(R.id.spectrum_chart);
        spectrumChart.setVisibility(View.VISIBLE);


        YAxis leftAxis = spectrumChart.getAxisLeft();
        leftAxis.setAxisMaximum(1f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawZeroLine(false);

        spectrumChart.getAxisRight().setEnabled(false);
        spectrumChart.setPinchZoom(true);


        LineDataSet set1;

        if (spectrumChart.getData() == null) {

            // region create a dataset and give it a type
            set1 = new LineDataSet(values, "Spectral Intensities");
            set1.setDrawValues(false);
            set1.setDrawIcons(false);
            set1.setDrawCircleHole(false);
            set1.setDrawCircles(false);

            set1.setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            set1.setLineWidth(1.25f);
            set1.setFillColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            set1.setFillAlpha(128);
            set1.setDrawFilled(true);

            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            // create a data object with the datasets
            LineData data = new LineData(set1);

            // set data
            spectrumChart.setData(data);
            spectrumChart.invalidate();
            //endregion
        }

        findViewById(R.id.color).setBackgroundColor(rgbColor);
        spectrumChart.setVisibility(View.VISIBLE);

        set1 = (LineDataSet) spectrumChart.getData().getDataSetByIndex(0);
        set1.setValues(values);
        spectrumChart.getData().notifyDataChanged();
        spectrumChart.notifyDataSetChanged();
        spectrumChart.invalidate();
    }


}
