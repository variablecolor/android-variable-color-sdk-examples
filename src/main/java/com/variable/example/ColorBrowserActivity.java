package com.variable.example;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.variable.error.VariableException;
import com.variable.example.fragment.SearchResultFragment;
import com.variable.product.SearchResult;
import com.variable.search.ProductSearch;

import java.util.List;

/**
 * Created by austinharris on 5/9/18.
 */

public class ColorBrowserActivity extends AppCompatActivity {

    private final SearchResultFragment mSearchResultFragment = new SearchResultFragment();


    //region Lifecycle Methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_color_browser);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        // Start the filter selection.
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, mSearchResultFragment, "SEARCH_RESULT")
                .commit();

        sendBlankSearch();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void sendBlankSearch() {
        new ProductSearch()
                .setSearchTerm("")
                .setSkip(0)
                .setLimit(75)
                .execute(this::onSearchComplete, this::onError);
    }

    /**
     * @param products
     */
    public void onSearchComplete(ProductSearch search, List<SearchResult> products) {
        mSearchResultFragment.onSearchComplete(search, products);
    }

    public void onError(@NonNull VariableException ex) {
    }

    //endregion
}
