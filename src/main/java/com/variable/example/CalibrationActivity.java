package com.variable.example;

import android.graphics.drawable.ColorDrawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewParent;
import android.widget.Toast;

import com.variable.bluetooth.ColorInstrument;
import com.variable.bluetooth.OnCalibrationResultListener;
import com.variable.bluetooth.OnColorCaptureListener;
import com.variable.color.ColorScan;
import com.variable.error.OnErrorListener;
import com.variable.error.VariableException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CalibrationActivity extends AppCompatActivity {
    private ColorScan greenTileScan, blueTileScan, whiteTileScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }


        ColorInstrument instrument = requireColorInstrument();
        if (instrument.getCalibrationScanCount() == 1){
            findViewById(R.id.container_spectro_calibration).setVisibility(View.GONE);
            findViewById(R.id.container_muse_calibration).setVisibility(View.VISIBLE);
        } else if (instrument.getCalibrationScanCount() == 3) {
            findViewById(R.id.container_spectro_calibration).setVisibility(View.VISIBLE);
            findViewById(R.id.container_muse_calibration).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.btnPerformCalibration).setOnClickListener(this::handleSpectroCalibration);
        findViewById(R.id.btnMuseCalibrate).setOnClickListener(this::handleMuseCalibration);
        findViewById(R.id.btnBlueTileScan).setOnClickListener(this::handleBlueTileScan);
        findViewById(R.id.btnWhiteTileScan).setOnClickListener(this::handleWhiteTileScan);
        findViewById(R.id.btnGreenTileScan).setOnClickListener(this::handleGreenTileScan);
    }

    @Override
    protected void onPause() {
        super.onPause();

        findViewById(R.id.btnPerformCalibration).setOnClickListener(null);
        findViewById(R.id.btnMuseCalibrate).setOnClickListener(null);
        findViewById(R.id.btnBlueTileScan).setOnClickListener(null);
        findViewById(R.id.btnWhiteTileScan).setOnClickListener(null);
        findViewById(R.id.btnGreenTileScan).setOnClickListener(null);
    }

    private void handleGreenTileScan(View view) {
        requireColorInstrument().requestColorScan(new OnColorCaptureListener() {
            @Override
            public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {

            }

            @Override
            public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                CalibrationActivity.this.greenTileScan = scan;
                findViewById(R.id.preview_green_tile).setBackgroundColor(scan.toColor());
            }
        }, this::handleScanError);
    }

    private void handleWhiteTileScan(View view) {
        requireColorInstrument().requestColorScan(new OnColorCaptureListener() {
            @Override
            public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                CalibrationActivity.this.whiteTileScan = scan;

                findViewById(R.id.preview_white_tile).setBackgroundColor(scan.toColor());
            }

            @Override
            public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                CalibrationActivity.this.whiteTileScan = scan;

                findViewById(R.id.preview_white_tile).setBackgroundColor(scan.toColor());

                findViewById(R.id.btnGreenTileScan).setEnabled(true);
                findViewById(R.id.btnBlueTileScan).setEnabled(true);
            }
        }, this::handleScanError);

    }

    private void handleBlueTileScan(View view) {
        requireColorInstrument().requestColorScan(new OnColorCaptureListener() {
            @Override
            public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {

            }

            @Override
            public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                findViewById(R.id.preview_blue_tile).setBackgroundColor(scan.toColor());
                CalibrationActivity.this.blueTileScan = scan;
            }
        }, this::handleScanError);

    }

    private void handleScanError(VariableException ex) {
        new AlertDialog.Builder(this)
                .setTitle("Scan Error")
                .setMessage(String.format("CODE: %s\nMessage: %s", ex.getCode(), ex.getMessage()))
                .show();
    }

    private void handleSpectroCalibration(View view) {
        if (whiteTileScan != null) {
            getColorInstrument().setCalibrationScans(
                    Arrays.asList(this.whiteTileScan, this.greenTileScan, this.blueTileScan),
                    (peripheral, ex) -> {
                        if (ex == null) {
                            clearTileScans();

                            Toast.makeText(CalibrationActivity.this, "Calibration is successful", Toast.LENGTH_SHORT).show();
                        } else {
                            this.handleScanError(ex);
                        }
                    });
        }
    }

    private void clearTileScans() {
        this.whiteTileScan = null;
        this.greenTileScan = null;
        this.blueTileScan = null;

        findViewById(R.id.preview_white_tile).setBackgroundResource(R.color.colorSecondaryDark);
        findViewById(R.id.preview_green_tile).setBackgroundResource(R.color.colorSecondaryDark);
        findViewById(R.id.preview_blue_tile).setBackgroundResource(R.color.colorSecondaryDark);
    }


    private void handleMuseCalibration(View view) {
        requireColorInstrument().requestCalibration(new OnColorCaptureListener() {
            @Override
            public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                colorInstrument.setCalibrationScans(
                        Arrays.asList(scan),
                        (peripheral, ex) -> {
                            if (ex == null) {
                                Toast.makeText(CalibrationActivity.this, "Calibration is successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(CalibrationActivity.this, "Calibration Error ", Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            @Override
            public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                colorInstrument.setCalibrationScans(
                        Arrays.asList(scan),
                        (peripheral, ex) -> {
                            if (ex == null) {
                                Toast.makeText(CalibrationActivity.this, "Calibration is successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(CalibrationActivity.this, "Calibration Error ", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }, this::handleScanError);

    }


    @NonNull
    public ColorInstrument requireColorInstrument() {
        return Objects.requireNonNull(getColorInstrument(), "color instrument is null or not connected, but was required to be connected");
    }
    @Nullable
    private ColorInstrument getColorInstrument() {
        return ((SampleApplication) getApplication()).getVariableSDK().getConnectionManager().getConnectedPeripheral();
    }

}
