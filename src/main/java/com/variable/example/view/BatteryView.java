package com.variable.example.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;

import com.variable.example.R;

public class BatteryView extends View {
    private Float radius = 0f;
    private Boolean isCharging = false;

    // Top
    // I only want to corner top-left and top-right so I use PaintDrawable instead of Paint
    private PaintDrawable topPaint;
    private Rect topRect = new Rect();
    private final int topPaintWidthPercent = 50;
    private final int topPaintHeightPercent = 8;

    // Border
    private Paint borderPaint;

    private RectF borderRect = new RectF();
    private final int borderStrokeWidthPercent = 8;
    private float borderStroke = 0f;

    // Percent
    private final Paint percentPaint = new Paint();
    private RectF percentRect = new RectF();
    private float percentRectTopMin = 0f;
    private int percent = 0;

    // Charging
    private RectF chargingRect = new RectF();
    private Bitmap chargingBitmap = null;


    //#region Constructors
    public BatteryView(Context context) {
        super(context);

        this.init(context, null);
    }

    public BatteryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }

    public BatteryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context, attrs);
    }

    public BatteryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context, attrs);
    }
    //#endregion

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        int color = ContextCompat.getColor(context, R.color.colorPrimary);
        topPaint = new PaintDrawable(color);
        borderPaint = new Paint();
        borderPaint.setColor(color);
        borderPaint.setStyle(Paint.Style.STROKE);

        chargingBitmap = getBitmap(context, R.drawable.ic_charging, 0, 0);

        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BatteryView);
            try {
                percent = ta.getInt(R.styleable.BatteryView_bv_percent, 0);
                isCharging = ta.getBoolean(R.styleable.BatteryView_bv_charging, false);
            } finally {
                ta.recycle();
            }
        }
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int  measureWidth = View.getDefaultSize(getSuggestedMinimumHeight(), widthMeasureSpec);
        int measureHeight = Math.round(measureWidth * 1.8f);
        setMeasuredDimension(measureWidth, measureHeight);

        radius = borderStroke / 2;
        borderStroke = (borderStrokeWidthPercent * measureWidth * 1.0f) / 100;

        // Top
        int topLeft = measureWidth * ((100 - topPaintWidthPercent) / 2) / 100;
        int topRight = measureWidth - topLeft;
        int topBottom = topPaintHeightPercent * measureHeight / 100;
        topRect = new Rect(topLeft, 0, topRight, topBottom);

        // Border
        float borderLeft = borderStroke / 2;
        float borderTop = topBottom*1.0f + borderStroke / 2;
        float borderRight = measureWidth - borderStroke / 2;
        float borderBottom = measureHeight - borderStroke / 2;
        borderRect = new RectF(borderLeft, borderTop, borderRight, borderBottom);

        // Progress
        float progressLeft = borderStroke;
        percentRectTopMin = topBottom + borderStroke;
        float progressRight = measureWidth - borderStroke;
        float progressBottom = measureHeight - borderStroke;
        percentRect = new RectF(progressLeft, percentRectTopMin, progressRight, progressBottom);

        // Charging Image
        float chargingLeft = borderStroke;
        float chargingTop = topBottom + borderStroke;
        float chargingRight = measureWidth - borderStroke;
        float chargingBottom = measureHeight - borderStroke;
        float diff = ((chargingBottom - chargingTop) - (chargingRight - chargingLeft));
        chargingTop += diff / 2;
        chargingBottom -= diff / 2;
        chargingRect = new RectF(chargingLeft, chargingTop, chargingRight, chargingBottom);
    }
    //#region Drawing
    @Override
    public void onDraw(Canvas canvas) {
        drawTop(canvas);
        drawBody(canvas);
        if (!isCharging) {
            drawProgress(canvas, percent);
        } else {
            drawCharging(canvas);
        }
    }

    public void drawTop(Canvas canvas) {
        topPaint.setBounds(topRect);
        topPaint.setCornerRadii(new float[]{radius, radius, radius, radius, 0f, 0f, 0f, 0f});
        topPaint.draw(canvas);
    }

    protected void drawBody(Canvas canvas) {
        borderPaint.setStrokeWidth(borderStroke);
        canvas.drawRoundRect(borderRect, radius, radius, borderPaint);
    }

    protected void drawProgress( Canvas canvas, int percent) {
        percentPaint.setColor(getPercentColor(percent));
        percentRect.top = percentRectTopMin + (percentRect.bottom - percentRectTopMin) * (100 - percent) / 100;
        canvas.drawRect(percentRect, percentPaint);
    }

    protected void drawCharging(Canvas canvas) {
        if(chargingBitmap == null){
            return;
        }

        canvas.drawBitmap(
                chargingBitmap,
                (canvas.getWidth() / 2) - (chargingBitmap.getWidth() / 2),
                (canvas.getHeight() / 2) - (chargingBitmap.getHeight() / 2),
                null);
    }

    //#endregion

    public int getPercentColor(int percent) {
        if (percent > 50) {
            return Color.GREEN;
        }
        if (percent > 30) {
            return Color.YELLOW;
        }
        return Color.RED;
    }
    @Nullable
    private Bitmap getBitmap(@NonNull Context context, int drawableId, int desireWidth, int desireHeight) {
        Drawable drawable = AppCompatResources.getDrawable(context, drawableId);
        if (drawable == null) {
            return null;
        }

        if (desireHeight < 1) {
            desireHeight = drawable.getIntrinsicHeight();
        }

        if (desireWidth < 1) {
            desireWidth = drawable.getIntrinsicWidth();
        }
        Bitmap bitmap = Bitmap.createBitmap(desireWidth, desireHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void setCharging(boolean isCharging) {
        this.isCharging = isCharging;
        invalidate(); // can improve by invalidate(Rect)
    }


    public void setPercent(int percent) {
        if (percent > 100 || percent < 0) {
            return;
        }
        this.percent = percent;
        invalidate();
    }

    public int getPercent() {
        return percent;
    }
}