package com.variable.example;

import android.app.Application;
import com.variable.Variable;

/**
 * Created by coreymann on 4/24/18.
 */

public class SampleApplication extends Application {
    private Variable variableSDK;

    public Variable getVariableSDK() {
        return variableSDK;
    }

    public void setVariableSDK(Variable mVariableSDK) {
        this.variableSDK = mVariableSDK;
    }
}
