package com.variable.example;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.widget.Toast;
import com.variable.Configuration;
import com.variable.Variable;

public class InitializationActivity extends AppCompatActivity implements Variable.OnVariableColorInitializeListener {


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initialization);


        ActivityCompat.requestPermissions(
                this,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
        );

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        findViewById(R.id.btnInitialize).setOnClickListener(this::initSDK);

    }

    private AlertDialog mDialog;

    private void initSDK(View v) {
        // build the configuration object.
        Configuration config = new Configuration.Builder(getApplicationContext())

                // Api key is defined inside a variable.properties file.
                // This can be customized to however your application wants to pull the api key provided to you.
                .setDeveloperToken(BuildConfig.API_KEY)
                .create();

        mDialog = new AlertDialog.Builder(this)
                .setTitle("Init Variable SDK")
                .setMessage("Initializing...")
//                .setCancelable(false)
                .show();

        // Make a call to start an initialization for the configuration instance.
        // Listener reference is releases after invocation.
        Variable.initialize(config, this, variableException -> {
            Toast.makeText(InitializationActivity.this, "initialization error. Please check your api key", Toast.LENGTH_LONG).show();
            mDialog.dismiss();
        });
    }

    @Override
    public void onInitialize(@NonNull Variable sdk) {
        mDialog.dismiss();

        ((SampleApplication) getApplication()).setVariableSDK(sdk);

        startActivity(new Intent(this, ProductDownloadActivity.class));
        finish();
    }
}
