package com.variable.example.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapToJsonConverter {
    public static JsonObject mapToJsonObject(Map<String, Object> map) {
        Gson gson = new Gson();
        return gson.toJsonTree(map).getAsJsonObject();
    }
}
