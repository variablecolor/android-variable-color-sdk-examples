package com.variable.example.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.variable.color.BatchedLabColor;
import com.variable.color.Illuminants;
import com.variable.color.LabColor;
import com.variable.color.Observer;
import com.variable.search.ColorSearchTerm;

import java.util.List;

/**
 * Created by coreymann on 5/1/18.
 */

public class ManualEnteredColorSearchTerm implements ColorSearchTerm {
    //region Static Methods

    public static ColorSearchTerm randomize() {
        LabColor randomLab = ActivityUtil.generateRandomColor();
        return new ManualEnteredColorSearchTerm(randomLab);
    }

    //endregion


    private final LabColor labColor;

    public ManualEnteredColorSearchTerm(LabColor labColor) {
        this.labColor = labColor;
    }

    @NonNull
    @Override
    public LabColor getAdjustedLabColor() {
        return labColor;
    }


    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public List<BatchedLabColor> getBatchedLabColors(@NonNull Illuminants targetIlluminant, @NonNull Observer targetObserver) {
        return null;
    }

}
