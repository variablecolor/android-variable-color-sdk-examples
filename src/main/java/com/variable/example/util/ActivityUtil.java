package com.variable.example.util;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.variable.color.LabColor;
import com.variable.color.RGB;
import com.variable.example.R;
import com.variable.therma.controllers.BluetoothLeService;

import java.util.Collection;
import java.util.Random;

/**
 * Created by coreymann on 4/26/18.
 */

public class ActivityUtil {
    public static <T>  T getItemAt(Collection<T> items, int position) {
        int i = 0;
        for (T filter : items) {
            if (i++ == position) {
                return filter;
            }
        }
        return null;
    }


    public static boolean checkBluetooth(@NonNull FragmentActivity context) {
        // Does this device have le capabilities and bluetooth....
        if (!BluetoothLeService.hasBluetoothHardware(context)) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.bluetooth_compatibility_title)
                    .setMessage(R.string.bluetooth_compatibility_message)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            return false;
        }

        BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (!manager.getAdapter().isEnabled()) {
            //Else if added to handle bluetooth not being enable prior to starting the application.
            context.startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
            return false;
        }

        return true;
    }
    public static boolean checkPermissions(@NonNull FragmentActivity context) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            boolean hasAccessFineLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            boolean hasCoarseLocation = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

            // Check for access on phones running M or later...
            // This is required by android for le scanning.
            if ((!hasAccessFineLocation || !hasCoarseLocation)) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.permission_request_title)
                        .setMessage(context.getString(R.string.permission_request_message))
                        .setPositiveButton(android.R.string.ok, (dialog, which) -> ActivityCompat.requestPermissions(
                                context,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                1000
                        )).show();
                return false;
            }
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            boolean hasConnectAccess = ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED;
            boolean hasScanAccess = ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_SCAN) == PackageManager.PERMISSION_GRANTED;
            if (!hasScanAccess || !hasConnectAccess) {
                ActivityCompat.requestPermissions(
                        context,
                        new String[]{
                                Manifest.permission.BLUETOOTH_CONNECT,
                                Manifest.permission.BLUETOOTH_SCAN
                        },
                        1000
                );
            }
        }
        return true;
    }

    public static boolean checkPermissionsAndFeatures(@NonNull FragmentActivity context) {
        if (!checkPermissions(context)) {
            return false;
        }


        if (!checkBluetooth(context)) {
            return false;
        }

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            PackageManager pm = context.getPackageManager();
            if (pm.hasSystemFeature(PackageManager.FEATURE_LOCATION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                if (locationManager == null || !(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.location_services_not_enabled_dialog_title)
                            .setMessage(R.string.location_services_required_dialog_message)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                    return false;
                }
            }
        }

        return true;
    }

    public static LabColor generateRandomColor() {
        final Random mRandom = new Random(System.currentTimeMillis());

        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.BLACK;

        final int red = (Color.red(baseColor) + mRandom.nextInt(256)) / 2;
        final int green = (Color.green(baseColor) + mRandom.nextInt(256)) / 2;
        final int blue = (Color.blue(baseColor) + mRandom.nextInt(256)) / 2;

        return new RGB(Color.rgb(red, green, blue)).toLAB();
    }


    public static String parseSerial(@NonNull String serial) {
        char[] serialChars  = serial.toCharArray();
        StringBuilder sb = new StringBuilder();

        for(int i = serialChars.length - 1; i >= 0; i -= 2) {
            sb.append(serialChars[i-1]);
            sb.append(serialChars[i]);
            if (i > 2) {
                sb.append(":");
            }
        }

        return sb.toString();
    }
}
