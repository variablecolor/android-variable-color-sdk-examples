package com.variable.example.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.InputStream;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private ImageView bmImage;
    private ProgressBar progress;
    private boolean isRunning;

    public DownloadImageTask(@NonNull  ImageView bmImage, @NonNull ProgressBar progress) {
        this.progress = progress;
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        this.isRunning = true;

        String urldisplay = urls[0];
        Bitmap bmp = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bmp;
    }

    @UiThread
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress.setVisibility(View.VISIBLE);
        bmImage.setVisibility(View.GONE);
    }

    @UiThread
    @Override
    protected void onCancelled(Bitmap bitmap) {
        super.onCancelled(bitmap);

        // release reference to avoid Context memory leak.
        bmImage = null;
        progress = null;
    }

    public boolean isRunning(){
        return this.isRunning;
    }

    @UiThread
    protected void onPostExecute(Bitmap result) {
        progress.setVisibility(View.GONE);
        bmImage.setVisibility(View.VISIBLE);
        bmImage.setImageBitmap(result);

        this.isRunning = false;

        // release reference to avoid Context memory leak.
        bmImage = null;
        progress = null;
    }
}

