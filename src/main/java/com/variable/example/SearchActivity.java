package com.variable.example;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.variable.bluetooth.ColorInstrument;
import com.variable.bluetooth.OnColorCaptureListener;
import com.variable.color.ColorScan;
import com.variable.error.OnErrorListener;
import com.variable.error.VariableException;
import com.variable.example.fragment.SearchFilterSelectionFragment;
import com.variable.example.fragment.SearchResultFragment;
import com.variable.example.util.ManualEnteredColorSearchTerm;
import com.variable.product.SearchResult;
import com.variable.search.ColorSearchTerm;
import com.variable.search.OnSearchCompleteListener;
import com.variable.search.ProductSearch;

import java.util.List;

/**
 * Created by coreymann on 5/1/18.
 */
public class SearchActivity extends AppCompatActivity implements OnErrorListener, OnSearchCompleteListener {
    private static final int STATE_FILTER_SELECTION = 0;
    private static final int STATE_SEARCH = 1;

    //region Intent Creations
    private static final String SEARCH_TYPE = "search_type";
    public static final String SCAN_AND_SEARCH = "scan_and_search";
    public static final String RANDOMIZE_COLOR_SEARCH = "randomize_search";

    /**
     * @param clazz - the activity class that is will be transitioned away from.
     * @return an intent ready to start an activity for scanning and searching.
     */
    public static Intent createScanAndSearchIntent(AppCompatActivity clazz) {
        Intent intent = new Intent(clazz, SearchActivity.class);
        intent.putExtra(SEARCH_TYPE, SCAN_AND_SEARCH);
        return intent;
    }


    /**
     * @param clazz - the activity class that is will be transitioned away from.
     * @return an intent ready to start an activity for scanning and searching.
     */
    public static Intent createRandomizedColorSearchIntent(AppCompatActivity clazz) {
        Intent intent = new Intent(clazz, SearchActivity.class);
        intent.putExtra(SEARCH_TYPE, RANDOMIZE_COLOR_SEARCH);
        return intent;
    }
    //endregion

    private int state = STATE_FILTER_SELECTION;

    private final SearchFilterSelectionFragment mSearchFilterFragment = new SearchFilterSelectionFragment();
    private final SearchResultFragment mSearchResultFragment = new SearchResultFragment();

    //region Lifecycle Methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filters);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        findViewById(R.id.btnNext).setOnClickListener(this::onButtonClick);

        // Start the filter selection.
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, mSearchFilterFragment)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        findViewById(R.id.btnNext).setOnClickListener(null);
    }

    @Override
    public void onBackPressed() {
        // Allows for backwards navigation to filters
        if (mSearchResultFragment.isAdded()) {
            state = STATE_FILTER_SELECTION;
            ((Button) findViewById(R.id.btnNext)).setText("Next");
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(mSearchResultFragment)
                    .add(R.id.frame, mSearchFilterFragment)
                    .commit();
        } else {
            super.onBackPressed();
        }
    }

    //endregion

    private void onButtonClick(View view) {
        // Change to the Product Search fragment
        if (state == STATE_FILTER_SELECTION) {
            state = STATE_SEARCH;
            ((Button) findViewById(R.id.btnNext)).setText(getButtonText());
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(mSearchFilterFragment)
                    .add(R.id.frame, mSearchResultFragment)
                    .commit();
            return;
        }


        switch (getSearchType()) {
            //region Perform a Color Scan
            case SCAN_AND_SEARCH: {

                findViewById(R.id.frame).setVisibility(View.GONE);
                findViewById(R.id.container).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.text_status)).setText("Scanning Color");

                // Locate the Colorimeter to make a request
                ColorInstrument colorInstrument = ((SampleApplication) getApplication()).getVariableSDK().getConnectionManager().getConnectedPeripheral();
                if (colorInstrument == null) {
                    Toast.makeText(this, "not connected", Toast.LENGTH_SHORT).show();
                    return;
                }


                // request a color scan and start a search store.
                colorInstrument.requestColorScan(new OnColorCaptureListener() {
                    @Override
                    public void onColorCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                        search(scan);
                    }

                    @Override
                    public void onSpectrumCapture(@NonNull ColorInstrument colorInstrument, @NonNull ColorScan scan) {
                        search(scan);
                    }
                }, this);
                break;
            }
            //endregion

            //region Randomize Color Search
            case RANDOMIZE_COLOR_SEARCH: {
                // Generates a randomly highly saturated ColorSearch Term.

                // This demonstrates, where pre-existing colors outside of the Variable SDK can be used for
                // color searching
                search(ManualEnteredColorSearchTerm.randomize());
                break;
            }

            //endregion
        }

    }


    //region Accessors
    private String getSearchType() {
        return getIntent().getStringExtra(SEARCH_TYPE);
    }

    private String getButtonText() {
        if (state == STATE_FILTER_SELECTION) {
            return "Next";
        }

        switch (getSearchType()) {
            case RANDOMIZE_COLOR_SEARCH:
                return "Randomize & Search";

            case SCAN_AND_SEARCH:
                return "Scan & Search";
        }

        return "";
    }
    //endregion

    //region Implementations
    @Override
    public void onError(@NonNull VariableException ex) {

    }


    private void search(@Nullable ColorSearchTerm scan) {
        findViewById(R.id.frame).setVisibility(View.GONE);
        findViewById(R.id.container).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.text_status)).setText("Searching");

        new ProductSearch()
                .setColorSearchTerm(scan)
                .setSearchFilters(mSearchFilterFragment.getSelectedSearchFilterSet())
                .setSkip(0)
                .setLimit(75)
                .execute(this, null);
    }

    /**
     *
     * @param search
     * @param products - the result set from the search
     */
    @Override
    public void onSearchComplete(@NonNull ProductSearch search, @NonNull  List<SearchResult> products) {
        findViewById(R.id.frame).setVisibility(View.VISIBLE);
        findViewById(R.id.container).setVisibility(View.GONE);

        mSearchResultFragment.onSearchComplete(search, products);
    }

    //endregion
}
