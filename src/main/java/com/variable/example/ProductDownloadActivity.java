package com.variable.example;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.variable.search.ProductManager;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by austinharris on 5/1/18.
 */

public class ProductDownloadActivity extends AppCompatActivity implements ProductManager.DownloadListener {

    //Example Directory to export DB to.
//    private File EXPORT_REALM_PATH = new File(
//            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
//            "db.dat");

    //region permissions.
//    private static final int REQUEST_EXTERNAL_STORAGE = 1;
//    //Permissions to request.
//    private static String[] PERMISSIONS_STORAGE = {
//            Manifest.permission.READ_EXTERNAL_STORAGE,
//            Manifest.permission.WRITE_EXTERNAL_STORAGE
//    };
    //end region

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloader);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (((SampleApplication) getApplication()).getVariableSDK() == null) {
            startActivity(new Intent(this, InitializationActivity.class));
            finish();
            return;
        }

//        findViewById(R.id.btnImportRealm).setOnClickListener((v) -> {
//            try {
//                this.onImportProducts();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//
//        findViewById(R.id.btnExportRealm).setOnClickListener((v) -> {
//            this.onExportProducts();
//        });

        if (((SampleApplication) getApplication()).getVariableSDK().getProductManager().isDownloadRequired()) {
            findViewById(R.id.btnDownload).setVisibility(View.VISIBLE);
            findViewById(R.id.btnNext).setVisibility(View.GONE);
//            findViewById(R.id.btnExportRealm).setVisibility(View.GONE);

        } else {
            ((TextView) findViewById(R.id.txtDownload)).setText("No product updates available");
            findViewById(R.id.btnNext).setVisibility(View.VISIBLE);
            findViewById(R.id.btnDownload).setVisibility(View.VISIBLE);
//            findViewById(R.id.btnExportRealm).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.btnDownload).setOnClickListener(this::onDownloadProductsClick);
        findViewById(R.id.btnNext).setOnClickListener(this::onNextClicked);

    }

    @Override
    protected void onPause() {
        super.onPause();

        // Clean up our lambdas for handling clicks
        findViewById(R.id.btnDownload).setOnClickListener(null);
        findViewById(R.id.btnNext).setOnClickListener(null);
    }

    //region Product Downloading
    private AlertDialog mDialog;

    private void onDownloadProductsClick(View view) {

        mDialog = new AlertDialog.Builder(this)
                .setTitle("Downloading products")
                .setMessage("Checking internet connection")
                .setCancelable(false)
                .show();

        // Move this logic into a dialog or another activity for simplicity
        ((SampleApplication) getApplication()).getVariableSDK().getProductManager().download(
                this,
                variableException -> {
                    Toast.makeText(ProductDownloadActivity.this, "error while downloading", Toast.LENGTH_LONG).show();
                }
        );
    }

    @Override
    public void onProgress(int percent) {
        if(mDialog != null) {
            mDialog.setMessage(String.format(Locale.getDefault(), "%d%% downloaded", percent));
        }
    }




    @Override
    public void onComplete(boolean isSuccess) {
        mDialog.dismiss();
        mDialog = null;

        if(isSuccess){
            startActivity(new Intent(this, MainOptionsActivity.class));
        }
    }

    public void onNextClicked(View view) {
        startActivity(new Intent(this, MainOptionsActivity.class));
    }
}
