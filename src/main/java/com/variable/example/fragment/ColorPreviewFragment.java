package com.variable.example.fragment;


import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.variable.color.Colorable;
import com.variable.example.R;

public class ColorPreviewFragment extends Fragment {
    private Colorable mColorable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_color_preview, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        update(mColorable);
    }

    public void update(@Nullable  Colorable colorable){
        this.mColorable = colorable;

        // let's safeguard against calls while being detached.
        if(getView() == null){
            return;
        }

        if(mColorable == null) {
            getView().findViewById(R.id.view_color).setBackgroundColor(Color.TRANSPARENT);
            return;
        }

        getView().findViewById(R.id.view_color).setBackgroundColor(mColorable.toColor());
    }
}
