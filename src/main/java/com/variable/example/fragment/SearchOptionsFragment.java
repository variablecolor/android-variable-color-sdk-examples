package com.variable.example.fragment;

import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;
import com.variable.example.R;

import java.util.LinkedHashMap;


/**
 * Created by austinharris on 5/10/18.
 */

public class SearchOptionsFragment extends Fragment {
    private final LinkedHashMap<String, Boolean> sortMap = new LinkedHashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_options, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupSortKey(view, R.id.btnname, R.id.nameGroup, "Name");
        setupSortKey(view, R.id.btncode, R.id.codeGroup, "Code");
        setupSortKey(view, R.id.btnvendor, R.id.vendorGroup, "Vendor");
        setupSortKey(view, R.id.btnbrightness, R.id.brightnessGroup, "Brightness");
        setupSortKey(view, R.id.btnsaturation, R.id.saturationGroup, "Saturation");
        setupSortKey(view, R.id.btnhue, R.id.hueGroup, "Hue");


        view.setTag(R.id.nameRadioASC, "Code");
        view.setTag(R.id.nameRadioDESC, "Code");
        view.setTag(R.id.codeRadioASC, "Code");
        view.setTag(R.id.codeRadioDESC, "Code");
        view.setTag(R.id.vendorRadioASC, "Vender Order");
        view.setTag(R.id.vendorRadioDESC, "Vendor Order");
        view.setTag(R.id.brightnessRadioASC, "Brightness");
        view.setTag(R.id.brightnessRadioDESC, "Brightness");
        view.setTag(R.id.saturationRadioASC, "Saturation");
        view.setTag(R.id.saturationRadioDESC, "Saturation");
        view.setTag(R.id.hueRadioASC, "Hue");
        view.setTag(R.id.hueRadioDESC, "Hue");


    }

    private void onRadioClick(RadioGroup radioGroup, int i) {
        if (getView() == null) {
            return;
        }


        int selectedId = radioGroup.getCheckedRadioButtonId();
        String sortKey = (String) getView().getTag(selectedId);
        if (sortMap.containsKey(sortKey)) {
            sortMap.put(sortKey, !sortMap.get(sortKey));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ToggleButton name = getView().findViewById(R.id.btnname);
        name.setOnClickListener(null);
        RadioGroup nameGroup = getView().findViewById(R.id.nameGroup);
        nameGroup.setOnCheckedChangeListener(null);


        ToggleButton code = getView().findViewById(R.id.btncode);
        code.setOnClickListener(null);
        RadioGroup codeGroup = getView().findViewById(R.id.codeGroup);
        codeGroup.setOnCheckedChangeListener(null);


        ToggleButton vendor = getView().findViewById(R.id.btnvendor);
        vendor.setOnClickListener(null);
        RadioGroup vendorGroup = getView().findViewById(R.id.vendorGroup);
        vendorGroup.setOnCheckedChangeListener(null);


        ToggleButton brightness = getView().findViewById(R.id.btnbrightness);
        brightness.setOnClickListener(null);
        RadioGroup brightGroup = getView().findViewById(R.id.brightnessGroup);
        brightGroup.setOnCheckedChangeListener(null);


        ToggleButton saturation = getView().findViewById(R.id.btnsaturation);
        saturation.setOnClickListener(null);
        RadioGroup satGroup = getView().findViewById(R.id.saturationGroup);
        satGroup.setOnCheckedChangeListener(null);


        ToggleButton hue = getView().findViewById(R.id.btnhue);
        hue.setOnClickListener(null);
        RadioGroup hueGroup = getView().findViewById(R.id.hueGroup);
        hueGroup.setOnCheckedChangeListener(null);
    }

    public LinkedHashMap<String, Boolean> getSortableKeys() {
        return sortMap;
    }


    private void setupSortKey(View view, @IdRes int toggleButtonID, @IdRes int radioGroupID, @NonNull String sortKey) {
        ToggleButton togglButton = view.findViewById(toggleButtonID);
        togglButton.setTag(R.id.text_name, sortKey);

        RadioGroup radioButtonGroup = view.findViewById(radioGroupID);
        togglButton.setTag(radioButtonGroup);
        enableButtonGroup(radioButtonGroup, sortMap.containsKey(sortKey));


        // Set the listeners after the initial checked state is setup.
        // This will avoid falsely changing a the state...
        togglButton.setOnClickListener(this::onToggleClick);

    }

    public void enableButtonGroup(RadioGroup view, boolean enable) {

        for (int i = 0; i < view.getChildCount(); i++) {
            (view.getChildAt(i)).setEnabled(true);
            if (i == 0) {
                ((RadioButton) view.getChildAt(i)).setChecked(enable);
            }
        }


        view.setOnCheckedChangeListener(enable ? this::onRadioClick : null);
    }

    public void onToggleClick(View view) {
        boolean checked = ((ToggleButton) view).isChecked();
        RadioGroup radioGroup = (RadioGroup) view.getTag();
        String keyName = (String) view.getTag(R.id.text_name);

        if (checked) {
            sortMap.put(keyName, true);
            this.enableButtonGroup(radioGroup, true);

        } else {
            sortMap.remove(keyName);
            radioGroup.clearCheck();
            this.enableButtonGroup(radioGroup, false);
        }
    }
}
