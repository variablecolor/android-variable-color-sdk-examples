package com.variable.example.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.variable.example.R;
import com.variable.example.interfaces.OnFilterItemsChangedListener;
import com.variable.example.util.ActivityUtil;
import com.variable.search.SearchFilter;
import com.variable.search.SearchFilterSet;

import java.util.Collection;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Created by coreymann on 5/1/18.
 */

public class SearchFilterSelectionFragment extends Fragment {

    private RecyclerView recyclerView;
    private SearchFilterAdapter mAdapter;

    //region lifecycle methods

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.recyclerView = view.findViewById(R.id.recyclerView);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        if (mAdapter == null) {
            mAdapter = new SearchFilterAdapter(new SearchFilterSet());
        }

        this.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // clean up after ourselves
        this.recyclerView.setAdapter(null);
        this.recyclerView = null;
    }

    //endregion

    /**
     * @return the currently selected search filters.
     */
    public Collection<SearchFilter> getSelectedSearchFilterSet() {
        return mAdapter.filters.getSelectedFilters();
    }

    //region Filter Keys Adapter

    private static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final RecyclerView recyclerView;
        private final OnFilterItemsChangedListener listener;
        private final Collection<SearchFilter> filterSet;

        HeaderViewHolder(@NonNull View itemView, @NonNull Collection<SearchFilter> filterSet, @NonNull OnFilterItemsChangedListener listener) {
            super(itemView);
            this.filterSet = filterSet;
            recyclerView = itemView.findViewById(R.id.value_recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.VERTICAL, false));
            name = itemView.findViewById(R.id.text_name);
            this.listener = listener;

        }


        private void bind(@NonNull SearchFilter filter, @NonNull Collection<String> selectedFilterValues) {
            ValueFilterAdapter adapter = new ValueFilterAdapter(filter.getKey(), selectedFilterValues);
            adapter.setListener(listener);
            adapter.update(filter.getValues());
            recyclerView.setAdapter(adapter);

            name.setText(filter.getKey());
        }
    }


    private static class SearchFilterAdapter extends RecyclerView.Adapter<HeaderViewHolder> implements OnFilterItemsChangedListener {
        private final SearchFilterSet filters;

        SearchFilterAdapter(@NonNull SearchFilterSet filters) {
            this.filters = filters;

            // fetch all filters.
            filters.initialize(this::onFilterFetch);
        }


        //region OnFilterItemsChangedListener
        @Override
        public void onFilterSelected(@NonNull String filterKey, @NonNull String filterValue, boolean isSelected) {
            if (isSelected) {
                this.filters.add(filterKey, filterValue, this::onFilterFetch);
            } else {
                this.filters.remove(filterKey, filterValue, this::onFilterFetch);
            }
        }
        //endregion

        //region RecyclerView.Adapter implementation
        @NonNull
        @Override
        public HeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_search_filter_header, parent, false);
            return new HeaderViewHolder(root, filters.getSelectedFilters(), this);
        }

        @Override
        public void onBindViewHolder(@NonNull HeaderViewHolder holder, int position) {
            SearchFilter filter = Objects.requireNonNull(ActivityUtil.getItemAt(filters.getAvailableFilters(), position));
            Collection<String> selectedValues = filters.getSelectedValues(filter.getKey());
            holder.bind(filter, selectedValues);
        }

        @Override
        public int getItemCount() {
            return filters.getAvailableFilters().size();
        }

        //endregion

        void onFilterFetch(@Nullable SearchFilterSet filters) {
            notifyDataSetChanged();
        }
    }
    //endregion


    //region Filter Values Adapter

    private static class ValueViewHolder extends RecyclerView.ViewHolder{
        private final TextView name;
        private boolean isSelected;

        /**
         *
         * @param itemView
         * @param listener - a listener for click
         * @param key - shared key across the entire adapter.
         */
        ValueViewHolder(@NonNull View itemView, @NonNull OnFilterItemsChangedListener listener, @NonNull final String key) {
            super(itemView);
            this.name = itemView.findViewById(R.id.value_text_name);
            this.isSelected = false;
            itemView.setOnClickListener(v -> {
                isSelected = !isSelected;
                listener.onFilterSelected(key, name.getText().toString(), isSelected);
            });
        }

        public void bind(String filterValue, boolean isSelected) {
            this.isSelected = isSelected;

            name.setText(filterValue);

            //Color Selected Filter.
            if (isSelected) {
                name.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.colorAccent));
            } else {
                name.setBackgroundColor(Color.WHITE);
            }
        }
    }


    private static class ValueFilterAdapter extends RecyclerView.Adapter<ValueViewHolder> {
        private final Collection<String> availableValues;
        private OnFilterItemsChangedListener listener;
        private final String filterKey;
        private final Collection<String> selectedValues;

        ValueFilterAdapter(String filterKey, Collection<String> selectedValues) {
            this.filterKey = filterKey;
            this.availableValues = new TreeSet<>(String::compareToIgnoreCase);
            this.selectedValues = selectedValues;
        }

        public void update(Collection<String> filters) {
            this.availableValues.addAll(filters);
        }

        public void setListener(OnFilterItemsChangedListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public ValueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_value_filter_header, parent, false);
            return new ValueViewHolder(root, listener, filterKey);
        }


        @Override
        public void onBindViewHolder(@NonNull ValueViewHolder holder, int position) {
            String filterValue = ActivityUtil.getItemAt(availableValues, position);
            holder.bind(filterValue, selectedValues.contains(filterValue));
        }

        @Override
        public int getItemCount() {
            return availableValues.size();
        }
    }

    //endregion
}
