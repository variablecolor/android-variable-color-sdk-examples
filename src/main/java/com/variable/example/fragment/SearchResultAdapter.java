package com.variable.example.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.variable.example.R;
import com.variable.product.SearchResult;

import java.util.ArrayList;
import java.util.List;

public  class SearchResultAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {
    public List<SearchResult> getResults() {
        return mResults;
    }

    public  interface OnProductLongClickListener {
        void onProductLongClick(@NonNull SearchResult result);
    }


    private final List<SearchResult> mResults = new ArrayList<>();

    @Nullable
    private  OnProductLongClickListener longClickListener;

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_product, parent, false);
        return new SearchResultViewHolder(root);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull SearchResultViewHolder holder) {
        super.onViewAttachedToWindow(holder);

        // listen for click listener
        holder.itemView.setOnLongClickListener((View v) -> {
            SearchResult result = (SearchResult) holder.itemView.getTag();
            if (longClickListener != null) {
                longClickListener.onProductLongClick(result);
                return true;
            }
            return false;
        });
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        //perform the data bind.
        holder.bind(mResults.get(position));

        // set the tag of the main view to the bounded search result.....
        //allowing access to SearchResult on click listeners
        holder.itemView.setTag(mResults.get(position));
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull SearchResultViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        // clean up after ourselves
        holder.itemView.setOnClickListener(null);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    public void setLongClickListener(OnProductLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }
}