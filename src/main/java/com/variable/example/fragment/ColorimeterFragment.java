package com.variable.example.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.variable.bluetooth.*;
import com.variable.example.CalibrationActivity;
import com.variable.example.ConnectUsingSerialActivity;
import com.variable.example.DirectConnectActivity;
import com.variable.example.R;
import com.variable.example.SampleApplication;
import com.variable.example.util.ActivityUtil;
import com.variable.example.view.BatteryView;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by coreymann on 4/25/18.
 */
public class ColorimeterFragment extends Fragment implements OnDisconnectListener, OnButtonPressListener {
    private ConnectionManager connectionManager;

    //region lifecycle methods

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        SampleApplication app = (SampleApplication) context.getApplicationContext();
        this.connectionManager = app.getVariableSDK().getConnectionManager();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_colorimeter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //region Listeners to Start Connection Activities
        view.findViewById(R.id.btnDirectConnect).setOnClickListener(v -> {
            startActivity(new Intent(getContext(), DirectConnectActivity.class));
        });

        view.findViewById(R.id.btnLegacyConnect).setOnClickListener(v -> {
            startActivity(new Intent(requireContext(), ConnectUsingSerialActivity.class));
        });
        //endregion

        view.findViewById(R.id.btnDisconnect).setOnClickListener((v) -> {
            // Get the peripheral from the connection manager.
            ColorInstrument peripheral = this.connectionManager.getConnectedPeripheral();

            // Check connection status
            if (peripheral != null) {

                // Perform a disconnect......
                peripheral.disconnect();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        ColorInstrument colorInstrument = connectionManager.getConnectedPeripheral();
        if (colorInstrument == null || !colorInstrument.isConnected()) {
            showDeviceInfoContainer(false);
        } else {
            //register this fragment to listen for button presses and disconnects.
            colorInstrument.setDisconnectListener(this);
            colorInstrument.setButtonPressListener(this);

            showDeviceInfoContainer(true);
            updateDeviceInfo(colorInstrument);
        }
    }


    @Override
    public void onPause() {
        super.onPause();

        // Make sure to clean up after ourselves on pause.
        ColorInstrument colorInstrument = connectionManager.getConnectedPeripheral();
        if (colorInstrument != null) {
            updateDeviceInfo(colorInstrument);
            colorInstrument.setDisconnectListener(null);
            colorInstrument.setButtonPressListener(null);
        }
    }

    private void showDeviceInfoContainer(boolean show) {
        View root = Objects.requireNonNull(getView());
        if (show) {
            root.findViewById(R.id.container_connected).setVisibility(View.VISIBLE);
            root.findViewById(R.id.container_options).setVisibility(View.GONE);
        } else {
            root.findViewById(R.id.container_connected).setVisibility(View.GONE);
            root.findViewById(R.id.container_options).setVisibility(View.VISIBLE);
        }
    }

    private void updateDeviceInfo(ColorInstrument colorInstrument) {
        View root = Objects.requireNonNull(getView());

        ((TextView) root.findViewById(R.id.txtSerial)).setText(colorInstrument.getSerial());
        ((TextView) root.findViewById(R.id.txtFirmware)).setText(colorInstrument.getFirmwareText(getActivity()));
        colorInstrument.requestBattery((c, percentage, voltage) -> {
            Log.d("Variable-SDK", String.format("BATT: %.3f -  %d", voltage, percentage));
            View view = getView();
            if (view != null) {
                BatteryView battView = view.findViewById(R.id.battery_view);
                battView.setPercent(percentage);

                ((TextView) view.findViewById(R.id.text_voltage)).setText(String.format("%.2fV", voltage));
            }
        });

        colorInstrument.requestChargingIndication((c, isCharged, isCharging) -> {
            View view = getView();
            if (view != null) {
                BatteryView battView = view.findViewById(R.id.battery_view);
                battView.setCharging(isCharging);


            }
        });

        colorInstrument.requestSignalStrength(this::onSignalStrength);
    }

    private int getColor(@NonNull Context context, int strength) {
        if (strength < -80) {
            return Color.RED;
        } else if (strength > -50) {
            return Color.GREEN;
        } else {
            return Color.YELLOW;
        }
    }

    public void onSignalStrength(@NonNull ColorInstrument instrument, @NonNull Integer rssi) {
        View view = getView();
        if (view != null) {
            ((TextView) view.findViewById(R.id.text_strength)).setText(String.format("%d dbm", rssi));
            view.findViewById(R.id.strength_status)
                    .getBackground()
                    .setColorFilter(getColor(view.getContext(), rssi), PorterDuff.Mode.SRC_ATOP);

            view.getHandler().postDelayed(() -> {
                if (instrument != null && instrument.isConnected()) {
                    instrument.requestSignalStrength(ColorimeterFragment.this::onSignalStrength);
                }
            }, TimeUnit.SECONDS.toMillis(3));

        }
    }

    @Override
    public void onDisconnect(@NonNull BluetoothDevice device) {
        showDeviceInfoContainer(false);
    }

    @Override
    public void onButtonPress(@NonNull ColorInstrument colorInstrument) {
        new AlertDialog.Builder(getActivity())
                .setMessage("Button Press")
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }
    //endregion
}
