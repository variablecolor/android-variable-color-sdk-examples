package com.variable.example.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.variable.example.R;
import com.variable.product.ProductColor;
import com.variable.product.SearchResult;
import com.variable.search.ColorSearchTerm;
import com.variable.search.OnSearchCompleteListener;
import com.variable.search.ProductSearch;

import java.util.List;

/**
 * Created by coreymann on 5/2/18.
 */

public class SearchResultFragment extends Fragment implements OnSearchCompleteListener, SearchResultAdapter.OnProductLongClickListener {

    private final SearchResultAdapter mAdapter = new SearchResultAdapter();
    private ProductSearch currentSearch;
    private final ColorPreviewFragment mColorPreviewFragment = new ColorPreviewFragment();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);

        mAdapter.setLongClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        //clean up after ourselves
        mAdapter.setLongClickListener(null);
    }

    @Override
    public void onSearchComplete(@NonNull ProductSearch search, @NonNull List<SearchResult> products) {
        this.currentSearch = search;

        if (this.currentSearch.getColorSearchTerm() != null) {
            mColorPreviewFragment.update(currentSearch.getColorSearchTerm().getAdjustedLabColor());
            if (!mColorPreviewFragment.isAdded()) {
                getChildFragmentManager().beginTransaction()
                        .add(R.id.frame_color_preview, mColorPreviewFragment)
                        .commit();
            }
        }

//        https://stackoverflow.com/questions/31759171/recyclerview-and-java-lang-indexoutofboundsexception-inconsistency-detected-in
        // thus using old way of notifyDatasetChanged
        mAdapter.getResults().clear();
        mAdapter.getResults().addAll(products);
        mAdapter.notifyDataSetChanged();

        // safe gaurd against results coming back after a user has navigated away.
        if (getView() == null) {
            return;
        }

        if (products.size() == 0) {
            getView().findViewById(R.id.text_no_results).setVisibility(View.VISIBLE);
        } else {
            getView().findViewById(R.id.text_no_results).setVisibility(View.GONE);
        }
    }

    @Override
    public void onProductLongClick(@NonNull SearchResult result) {
        // Our SDK does not support text search in combination with color searching...
        if (TextUtils.isEmpty(currentSearch.getSearchTerm())) {
            if (result.getProduct().getProductColors().size() > 1) {
                buildAlertDialog(result);
            }else {
                sendColorSearch(result.getProduct().getProductColors().get(0));
            }
        }
    }

    private void buildAlertDialog(@NonNull SearchResult result) {
        final ColorSectorAdapter adapter = new ColorSectorAdapter(getActivity(), result.getProduct().getProductColors());
        new AlertDialog.Builder(getActivity())
                .setTitle("Select a color:")
                .setAdapter(adapter, (DialogInterface dialog1, int which) -> sendColorSearch(adapter.getItem(which)))
                .setNegativeButton("Cancel", null)
                .show();
    }

    public void sendColorSearch(@NonNull ColorSearchTerm result) {
        currentSearch.setSearchTerm(null)
                .setColorSearchTerm(result)
                .execute(this, null);
    }

    //region ColorSector Adapter
    private class ColorSectorAdapter extends ArrayAdapter<ProductColor> {
        ColorSectorAdapter(Context context, List<ProductColor> colors) {
            super(context, 0, colors);
        }

        @NonNull
        @Override
        public View getView(int pos, View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.cross_ref_item, parent, false);
            }


            ProductColor color = getItem(pos);
            if (color != null) {
                (convertView.findViewById(R.id.cross_view)).setBackgroundColor(color.toColor());
            }

            return convertView;
        }
    }
    //endregion

}
