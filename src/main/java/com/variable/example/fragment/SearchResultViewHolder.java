package com.variable.example.fragment;

import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.variable.example.R;
import com.variable.example.util.DownloadImageTask;
import com.variable.product.ProductAttribute;
import com.variable.product.SearchResult;

import java.util.List;
import java.util.Locale;

public class SearchResultViewHolder extends RecyclerView.ViewHolder {
    private final TextView name, code, deltaE, collection;
    private final ImageView image;
    private final ProgressBar progress;

    public SearchResultViewHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.text_name);
        code = itemView.findViewById(R.id.text_code);
        deltaE = itemView.findViewById(R.id.text_deltaE);
        collection = itemView.findViewById(R.id.text_collection);

        progress = itemView.findViewById(R.id.progress);

        image = itemView.findViewById(R.id.image_product);
    }

    void bind(@NonNull SearchResult result) {
        if(result.getProduct().getImages().isEmpty()){
            image.setImageDrawable(new ColorDrawable(result.getProduct().toColor()));

        } else {
            //region Image Loading
            if(image.getTag() != null){
                ((DownloadImageTask) image.getTag()).cancel(true);
            }

            // Create a new download task
            DownloadImageTask downloadImageTask = new DownloadImageTask(image, progress);

            // set the reference to the tag, to allow for cancellation when view detaches from window
            image.setTag(downloadImageTask);

            // Start the download....
            downloadImageTask.execute(result.getProduct().getImages().get(0));
            //endregion
        }

        code.setText("-");
        name.setText("-");
        collection.setText("-");

        List<ProductAttribute> attributes = result.getProduct().getAttributes();
        for (ProductAttribute attr : attributes) {
            if (attr.getKey().equals("code")) {
                code.setText(attr.getTranslatedValue());
            } else if (attr.getKey().equals("name")) {
                name.setText(attr.getTranslatedValue());
            } else if (attr.getKey().equals("collection")) {
                collection.setText(attr.getTranslatedValue());
            }
        }


        if (result.getDeltaE() == null) {
            deltaE.setText("");
        } else {
            deltaE.setText(String.format(Locale.getDefault(), "∆E: %.2f", result.getDeltaE()));
        }
    }
}
