package com.variable.example;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.variable.example.util.ActivityUtil;
import com.variable.therma.controllers.BlueGeckoController;
import com.variable.util.VariableUtil;

import java.util.Objects;

public class ConnectUsingSerialActivity extends AbstractConnectionHandlerActivity {


    private TextView txtStatus;
    private EditText editSerial;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connect_serial);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        this.txtStatus = findViewById(R.id.text_status);
        this.editSerial = findViewById(R.id.edit_serial);

        findViewById(R.id.btnStart).setOnClickListener(this::onConnectClick);
    }

    private void onConnectClick(@NonNull View view) {
        String serial = this.editSerial.getText().toString().toUpperCase();
        if(serial.length() != 12){
            txtStatus.setText(R.string.invalid_serial);
            return;
        }

        int deviceType = getSelectedDeviceType();
        if(deviceType == -1){
            txtStatus.setText("Select a device type before continuing");
            return;
        }

        try {

            BluetoothDevice device = requireBluetoothAdapter().getRemoteDevice(ActivityUtil.parseSerial(serial));

            /// A direct connection using a manually derived bluetooth device requires telling an internal bluetooth framework the device type of that bluetooth device the user has entered.
            requireVariable()
                    .getConnectionManager()
                    .getBluetoothLEService()
                    .putBluetoothDevice(device, deviceType);

            // make the call into the framework to connect the device.
            this.onDeviceSelect(device);

        } catch(IllegalArgumentException ex){
            Log.e(BuildConfig.TAG, "invalid serial and bluetooth address", ex);
            txtStatus.setText(R.string.invalid_serial);
        }
    }

    @NonNull
    private BluetoothAdapter requireBluetoothAdapter() {
        return Objects.requireNonNull(((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter(), "missing adapter");
    }

    private boolean isSelected(int resID){
        return ((RadioButton) findViewById(resID)).isChecked();
    }

    private int getSelectedDeviceType() {
        if (isSelected(R.id.radio_color_muse)) {
            return BlueGeckoController.Devices.COLOR_MUSE;
        }

        if (isSelected(R.id.radio_color_muse_pro)) {
            return BlueGeckoController.Devices.COLOR_MUSE_PRO;
        }

        if (isSelected(R.id.radio_spectro) || isSelected(R.id.radio_spectro_pro)) {
            return BlueGeckoController.Devices.SPECTRO;
        }

        if(isSelected(R.id.radio_color_muse_2)) {
            return BlueGeckoController.Devices.COLOR_MUSE_2;
        }
//        4C88DB570F54

        return -1;
    }


    @Override
    protected void onResume() {
        super.onResume();

        ActivityUtil.checkBluetooth(this);
    }
}
