package com.variable.example;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.variable.error.VariableException;
import com.variable.example.fragment.SearchOptionsFragment;
import com.variable.example.fragment.SearchResultFragment;
import com.variable.product.SearchResult;
import com.variable.search.ProductSearch;

import java.util.List;

/**
 * Created by austinharris on 5/3/18.
 */

public class TextSearchActivity extends AppCompatActivity {

    private final SearchResultFragment mSearchResultFragment = new SearchResultFragment();
    private final SearchOptionsFragment mSearchOptinsFragment = new SearchOptionsFragment();
    private String prefix;
    private String postfix;


    //region Lifecycle Methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_text_search);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        SearchView sv = findViewById(R.id.text_search_bar);

        findViewById(R.id.btnNext).setOnClickListener(this::onButtonClick);
        findViewById(R.id.btnSortAndSearch).setOnClickListener(this::showSortingFragment);

        ((Button) findViewById(R.id.btnNext)).setText("Search");

        // Start the filter selection.
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, mSearchResultFragment, "SEARCH_RESULT")
                .commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        findViewById(R.id.btnNext).setOnClickListener(null);
    }

    //endregion

    private void onButtonClick(View view) {
        if (mSearchOptinsFragment.isAdded()) {
            findViewById(R.id.btnSortAndSearch).setVisibility(View.VISIBLE);
            ((Button) findViewById(R.id.btnNext)).setText("Search");
            SearchView sv = findViewById(R.id.text_search_bar);
            sv.setVisibility(View.VISIBLE);
            getSupportFragmentManager().popBackStack();
            return;
        }

        SearchView sv = findViewById(R.id.text_search_bar);

        search(sv.getQuery().toString());
    }

    private void showSortingFragment(View view) {
        ((Button) findViewById(R.id.btnNext)).setText("Continue");
        findViewById(R.id.btnSortAndSearch).setVisibility(View.GONE);
        SearchView sv = findViewById(R.id.text_search_bar);
        sv.setVisibility(View.GONE);

        getSupportFragmentManager()
                .beginTransaction()
                .remove(mSearchResultFragment)
                .add(R.id.frame, mSearchOptinsFragment, "SEARCH_OPTIONS")
                .addToBackStack(null)
                .commit();
    }


    private void search(@NonNull String searchTerm) {
        // Wildcard characters can be used for searching.
        /* Condition that the value of field matches with the specified substring, with wildcards:
         * '*' matches [0, n] unicode chars
         * '?' matches a single unicode char.
         **/
        new ProductSearch()
                .setSearchTerm("*" + searchTerm + "*")
                .setSkip(0)
                .setLimit(20)
                .setSortOrder(mSearchOptinsFragment.getSortableKeys())
                .execute(this::onSearchComplete, this::onError);
    }

    /**
     * @param products
     */
    public void onSearchComplete(ProductSearch search, List<SearchResult> products) {
        mSearchResultFragment.onSearchComplete(search, products);
    }

    public void onError(@NonNull VariableException ex) {

    }

}
