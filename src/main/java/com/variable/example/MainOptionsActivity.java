package com.variable.example;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.variable.bluetooth.ConnectionManager;
import com.variable.example.util.ActivityUtil;


public class MainOptionsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        if (((SampleApplication) getApplication()).getVariableSDK() == null) {
            startActivity(new Intent(this, InitializationActivity.class));
            finish();
            return;
        }

        setContentView(R.layout.activity_main_options);
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.btnScanAndSearch).setOnClickListener(this::onScanAndSearchClick);
        findViewById(R.id.btnRandomizeColorSearch).setOnClickListener(this::onRandomizeColorSearchClick);

        // Check current connection status.
        ConnectionManager manager = ((SampleApplication) getApplication()).getVariableSDK().getConnectionManager();
        if (manager.getConnectedPeripheral() != null) {
            findViewById(R.id.btnCalibration).setVisibility(View.VISIBLE);
            findViewById(R.id.btnCalibration).setOnClickListener(v -> {
                startActivity(new Intent(this, CalibrationActivity.class));
            });


            findViewById(R.id.btnScanAndSearch).setVisibility(View.VISIBLE);
            findViewById(R.id.btnScanColors).setVisibility(View.VISIBLE);
            findViewById(R.id.btnScanColors).setOnClickListener((v) -> {
                startActivity(new Intent(this, ColorScanningActivity.class));
            });
            findViewById(R.id.btnTextSearch).setOnClickListener((v) -> {
                startActivity(new Intent(this, TextSearchActivity.class));
            });
            findViewById(R.id.btnBrowseColor).setOnClickListener((v) -> {
                startActivity(new Intent(this, ColorBrowserActivity.class));
            });

        } else {
            findViewById(R.id.btnTextSearch).setOnClickListener((v) -> {
                startActivity(new Intent(this, TextSearchActivity.class));
            });
            findViewById(R.id.btnBrowseColor).setOnClickListener((v) -> {
                startActivity(new Intent(this, ColorBrowserActivity.class));
            });
            findViewById(R.id.btnScanAndSearch).setVisibility(View.GONE);
            findViewById(R.id.btnScanColors).setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1000){
            if (ActivityUtil.checkPermissionsAndFeatures(this)) {
                startActivity(new Intent(this, DirectConnectActivity.class));
            }
        }
    }


    private void onRandomizeColorSearchClick(View view) {
        startActivity(SearchActivity.createRandomizedColorSearchIntent(this));
    }


    @Override
    protected void onPause() {
        super.onPause();

        // Clean up our lambdas for handling clicks
        findViewById(R.id.btnScanAndSearch).setOnClickListener(null);
        findViewById(R.id.btnScanColors).setOnClickListener(null);
    }

    //region onClick Handlers
    private void onScanAndSearchClick(View view) {
        startActivity(SearchActivity.createScanAndSearchIntent(this));
    }
    //endregion
}

