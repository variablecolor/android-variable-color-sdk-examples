package com.variable.example.interfaces;

import androidx.annotation.NonNull;

/**
 * Created by austinharris on 5/2/18.
 */

public interface OnFilterItemsChangedListener {

    void onFilterSelected(@NonNull String filterKey, @NonNull String filterValue, boolean isSelected);
}
