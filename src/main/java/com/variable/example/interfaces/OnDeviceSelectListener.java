package com.variable.example.interfaces;

import android.bluetooth.BluetoothDevice;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;

/**
 * Created by coreymann on 4/25/18.
 */
public interface OnDeviceSelectListener {
    @UiThread
    void onDeviceSelect(@NonNull BluetoothDevice device);
}