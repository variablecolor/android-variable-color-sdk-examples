package com.variable.example;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanFilter;
import android.os.ParcelUuid;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.variable.Variable;
import com.variable.bluetooth.ColorInstrument;
import com.variable.bluetooth.DeviceConnectionListener;
import com.variable.error.BluetoothDiscoveryException;
import com.variable.error.CalibrationException;
import com.variable.error.DeviceBatchException;
import com.variable.error.NetworkException;
import com.variable.error.OnErrorListener;
import com.variable.error.ServiceDiscoveryException;
import com.variable.error.VariableException;
import com.variable.error.ZeroColorimetersDiscoveredException;
import com.variable.example.interfaces.OnDeviceSelectListener;
import com.variable.therma.controllers.BlueGeckoController;
import com.variable.therma.controllers.BluetoothLeService;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public abstract class AbstractConnectionHandlerActivity extends AppCompatActivity implements OnErrorListener, OnDeviceSelectListener, DeviceConnectionListener {


    //region Lifecycle Methods
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // The user is navigating away from this screen, alert the ConnectionManager to disposed of the current
        // connection attempt.
        ((SampleApplication) getApplication()).getVariableSDK().getConnectionManager().multiConnectStop();
    }
    //#endregion


    @NonNull
    protected TextView requireStatusView() {
        return Objects.requireNonNull((TextView) findViewById(R.id.text_status), "inherited activity must have a R.id.text_status view for updates");
    }

    @NonNull
    protected View requireControlButton() {
        return Objects.requireNonNull((View) findViewById(R.id.btnStart), "inherited activity must have a R.id.btnStart for user interaction with connection.");
    }


    @Override
    public void onStateChange(int previousState, int newState, Object data) {
        TextView txtStatus = requireStatusView();

        switch (newState) {
            case States.DEVICE_CONNECTION:
                txtStatus.setText(R.string.status_connecting);
                break;

            case States.AWAITING_CONFIRMATION:
                txtStatus.setText(R.string.status_connected);
                break;

            case States.FETCHING_CALIBRATION:
                if(previousState != newState) {
                    // Initial State Change into Fetching Calibration
                    txtStatus.setText(R.string.status_initialiizing_device);
                }else {
                    // First Time Connections always require a download that may take a minute
                    ///// depending on the user's internet speed
                    txtStatus.setText(String.format(Locale.getDefault(), "Fetched %d%% Device Data", data));
                }
                break;

            case States.DISCONNECTED:
                txtStatus.setText("Device Disconnected");
                break;
        }
    }


    @Override
    public void onError(@NonNull VariableException ex) {
        TextView txtStatus = requireStatusView();

        if (ex instanceof NetworkException) {
            new AlertDialog.Builder(this)
                    .setTitle("Internet Connectivity")
                    .setMessage("Failed to download device information on first time connection")
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            txtStatus.setText("Failed downloading device information on first time connections");

        } else if (ex instanceof BluetoothDiscoveryException) {
            int androidBluetoothDiscoveryError = ((BluetoothDiscoveryException) ex).getScanError();
            Toast.makeText(this, "Bluetooth Scan Error Code: " + androidBluetoothDiscoveryError, Toast.LENGTH_LONG).show();

        } else if(ex instanceof ZeroColorimetersDiscoveredException) {
            ZeroColorimetersDiscoveredException exception = (ZeroColorimetersDiscoveredException) ex;
            new AlertDialog.Builder(this)
                    .setTitle("Missing Bluetooth Devices")
                    .setMessage("Failed to find color sensors in discovery scan")
                    .setPositiveButton(android.R.string.ok, null)
                    .show();

        } else if (ex instanceof DeviceBatchException) {
            new AlertDialog.Builder(this)
                    .setTitle("Error during connection")
                    .setMessage("Please contact manufacturer about defective hardware. Serial Number: " + ((DeviceBatchException) ex).getSerial())
                    .setPositiveButton(android.R.string.ok, null)
                    .show();

        } else if (ex instanceof CalibrationException) {
            new AlertDialog.Builder(this)
                    .setTitle("Error during connection")
                    .setMessage("Please contact manufacturer about defective hardware. Serial Number: " + ((CalibrationException) ex).getSerial())
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        } else if(ex instanceof ServiceDiscoveryException) {
            new AlertDialog.Builder(this)
                    .setTitle("Error during connection")
                    .setMessage("The devices connected, but gatt profile is missing after service discovery.")
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }

        requireControlButton().setVisibility(View.VISIBLE);
    }

    @Override
    public void onDeviceSelect(@NonNull BluetoothDevice device) {

        // Grab the reference to the Variable SDK that was set during the
        // InitializationActivity.
        requireVariable().getConnectionManager().connect(device, this, this);
    }


    @NonNull
    protected Variable requireVariable() {
        return Objects.requireNonNull(((SampleApplication) getApplication()).getVariableSDK(), "Variable SDK is not initialized");
    }

    private static List<ScanFilter> buildScanFilters() {
        List<ScanFilter> filters = new ArrayList<>();

        // Scan Filter for Color Muse
        filters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.COLOR_MUSE)).build());

        // Scan Filter for Color Muse Pro with Single Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.COLOR_MUSE_PRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x01, 0x00, 0x00, 0x00}, new byte[]{0x01, 0x00, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Color Muse Pro with Double Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.COLOR_MUSE_PRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x02, 0x04, 0x00, 0x00}, new byte[]{0x01, 0x01, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Color Muse Pro with Single Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.COLOR_MUSE_PRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x01, 0x04, 0x00, 0x00}, new byte[]{0x01, 0x01, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Spectro One and Spectro One Pro with Double Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.SPECTRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x02, 0x00, 0x00, 0x00}, new byte[]{0x01, 0x00, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Spectro One and Spectro One Pro with Single Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.SPECTRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x01, 0x00, 0x00, 0x00}, new byte[]{0x01, 0x00, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Spectro One Pro with Single Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.SPECTRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x01, 0x03, 0x00, 0x00}, new byte[]{0x01, 0x01, 0x00, 0x00})
                        .build()
        );


        // Scan Filter for Spectro One Pro with Double Click only detection
        filters.add(
                new ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid.fromString(BluetoothLeService.GattServices.SPECTRO))
                        .setManufacturerData(0x2FF, new byte[] { 0x02, 0x03, 0x00, 0x00}, new byte[]{0x01, 0x01, 0x00, 0x00})
                        .build()
        );


        return filters;
    }

    @Override
    public void onDeviceReady(@NonNull ColorInstrument colorInstrument) {
        setResult(RESULT_OK);
        finish();
    }

}
