package com.variable.example;

import android.bluetooth.BluetoothDevice;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.variable.Variable;

import com.variable.bluetooth.OnDiscoveryListener;

import com.variable.example.interfaces.OnDeviceSelectListener;
import com.variable.example.util.ActivityUtil;
import com.variable.util.VariableUtil;


import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by coreymann on 4/25/18.
 */

public class DirectConnectActivity extends AbstractConnectionHandlerActivity implements OnDiscoveryListener {

    //region Members
    private TextView txtStatus;
    private Button btnControl;
    private ProgressBar progress;
    private RecyclerView recyclerView;

    //region lifecycle methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_connect);

        // Check for permissions like bluetooth, location-services...
        ActivityUtil.checkPermissionsAndFeatures(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        }

        progress = findViewById(R.id.progress);
        btnControl = findViewById(R.id.btnStart);
        txtStatus = findViewById(R.id.text_status);
        recyclerView = findViewById(R.id.list_devices);


        ((RecyclerView) findViewById(R.id.list_devices)).setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        btnControl.setOnClickListener(this::scanForDevices);

        scanForDevices(btnControl);
    }


    //endregion

    private void scanForDevices(View v) {
        v.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        recyclerView.setAdapter(null);

        // Grab the reference to the Variable SDK that was set during the
        // InitializationActivity.
        Variable sdk = ((SampleApplication) getApplication()).getVariableSDK();

        long discoverMillis = TimeUnit.SECONDS.toMillis(5);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            discoverMillis = TimeUnit.SECONDS.toMillis(15);
        }

        // Check for permissions like bluetooth, location-services...
        ActivityUtil.checkPermissionsAndFeatures(this);


        // This will cancel any existing multi connection attempt in progress.
        sdk.getConnectionManager().discoverBluetoothDevices(this, this, discoverMillis);
    }



    @Override
    public void onDiscoveryComplete(@NonNull List<BluetoothDevice> devices) {
        txtStatus.setText("Choose a device");
        btnControl.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(new DeviceAdapter(this, devices));

        if (devices.size() == 0) {
            txtStatus.setText(R.string.no_devices_found);
        }
    }


    //region Adapter and View Holders

    private static final class DeviceViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;

        public DeviceViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.text_name);
        }
    }

    private static final class DeviceAdapter extends RecyclerView.Adapter<DeviceViewHolder> {
        private final List<BluetoothDevice> mDevices;
        private final OnDeviceSelectListener onDeviceSelectListener;

        private DeviceAdapter(OnDeviceSelectListener connMan, List<BluetoothDevice> mDevices) {
            this.mDevices = mDevices;
            this.onDeviceSelectListener = connMan;
        }

        @NonNull
        @Override
        public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new DeviceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_device, parent, false));
        }


        @Override
        public void onViewRecycled(@NonNull DeviceViewHolder holder) {
            super.onViewRecycled(holder);

            //Cleanup references
            holder.itemView.setOnClickListener(null);
        }

        @Override
        public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
            BluetoothDevice device = mDevices.get(position);
            holder.name.setText(VariableUtil.parseString(device));

            holder.itemView.setOnClickListener((v) -> {
                onDeviceSelectListener.onDeviceSelect(device);
            });
        }

        @Override
        public int getItemCount() {
            return mDevices.size();
        }
    }

    //endregion
}
